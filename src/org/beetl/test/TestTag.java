package org.beetl.test;

import java.io.IOException;


import org.beetl.core.tag.Tag;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class TestTag extends Tag {

	@Override
	public void render() {
		try {
			this.ctx.byteWriter.writeString("tag");
		} catch (IOException e) {
			
		}

	}

}
