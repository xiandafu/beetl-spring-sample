package org.beetl.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.core.om.DefaultAAFactory;


public class SortFunction implements Function {
	DefaultAAFactory defaultAAFactory = new DefaultAAFactory();
	@Override
	public Object call(Object[] arg, Context arg1) {

		List cols = (List)arg[0];

		String attrName = (String)arg[1];
		sort(cols,attrName);
		return cols;
	}
	
	private void sort(List cols,final String name){
		
		Collections.sort(cols, new Comparator() {
			
			public int compare(Object o1, Object o2) {
				Object v1 = defaultAAFactory.buildFiledAccessor(o1.getClass()).value(o1,name);
				Object v2 = defaultAAFactory.buildFiledAccessor(o2.getClass()).value(o2,name);

				if(v1 instanceof Comparable &&v2 instanceof Comparable){
					return ((Comparable)v1).compareTo(v2);
				}else{
					throw new RuntimeException("���ܱȽ�");
				}
				
			}
		});
	}
	
	
	

}
