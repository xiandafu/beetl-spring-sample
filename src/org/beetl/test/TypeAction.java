package org.beetl.test;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletRequest;

import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class TypeAction {
	@RequestMapping("/type.html")
	public String index(@RequestParam(value = "type", required = false, defaultValue = "") String type,
			@RequestParam(value = "forwordType", required = false, defaultValue = "") String forwordType,
			HttpServletRequest request) {
		if ((forwordType != null) && !"".equals(forwordType)) {
			type = forwordType;
		}
		
		if ("cms".equals(type)) {
			// type为cp时，使用classpath下的beetl视图解析器
			return "/cmstemplate/index";
		} else if ("jsp".equals(type)) {
			// type为jsp时，使用jsp视图解析器
			return "/jsp/index";
		} else if ("web".equals(type)) {
			// 其他情况使用beetl视图解析器
			return "/index";
		} else if ("redirect".equals(type)) {
			// 重定向
			return BeetlSpringViewResolver.redirect("/index.html?type=cms");
		} else if ("forward".equals(type)) {
			// 转发
			// 如果仍然用type传参，原来request的type参数也会被带过去
			return BeetlSpringViewResolver.forward("/index.html?forwordType=cms");
		} else {
			return BeetlSpringViewResolver.redirect("/index.html?type=cms");
		}
	}
	
	
}
