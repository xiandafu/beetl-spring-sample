package org.beetl.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class JsonAction {
	@RequestMapping("/json.html")
	@ResponseBody
	public List index() {
		List list = new ArrayList();
		list.add("\"hello");
		list.add(new ArrayList());		
		list.add(null);
		return list;
	}
	
	
}
