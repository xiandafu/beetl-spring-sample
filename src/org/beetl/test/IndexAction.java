package org.beetl.test;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class IndexAction {
	@RequestMapping("/index.html")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView();
		view.setViewName("/template/index");
		return view;
		
	}
	
	@RequestMapping("/resource.html")
	public String resource(
			HttpServletRequest request) {
		return "/template/index";
	}
	
	@RequestMapping(value="/model.html")
    public String say(Model model) {        
         model.addAttribute("hello", "hello world");  
         model.addAttribute("a");
         return "/template/model";
    } 
	

}
