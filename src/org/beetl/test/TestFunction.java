package org.beetl.test;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.stereotype.Service;

@Service
public class TestFunction implements Function {


	public Object call(Object[] paras, Context ctx) {
		return "a";
	}

}
